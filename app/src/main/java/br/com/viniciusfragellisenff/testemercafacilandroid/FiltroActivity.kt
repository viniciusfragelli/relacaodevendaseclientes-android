package br.com.viniciusfragellisenff.testemercafacilandroid

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.Filtro
import kotlinx.android.synthetic.main.activity_filtro.*
import java.text.SimpleDateFormat
import android.widget.DatePicker
import java.util.*


class FiltroActivity : AppCompatActivity() {

    private var filtro: Filtro = Filtro();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filtro)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Filtrar"
        supportActionBar?.setHomeButtonEnabled(true)
        filtro = intent.getSerializableExtra("filtro") as Filtro;
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.aplicar,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_aplicar -> {
                val simple = SimpleDateFormat("yyyy-MM-dd")
                filtro.inicio = simple.format(getDateFromDatePicker(dpInicio))
                filtro.fim = simple.format(getDateFromDatePicker(dpFim))
                var intent = intent
                MainActivity.filtro = filtro;
                intent.putExtra("filtro", filtro)
                setResult(Activity.RESULT_OK,intent)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getDateFromDatePicker(datePicker: DatePicker): java.util.Date {
        val day = datePicker.dayOfMonth
        val month = datePicker.month
        val year = datePicker.year

        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)

        return calendar.getTime()
    }


}
