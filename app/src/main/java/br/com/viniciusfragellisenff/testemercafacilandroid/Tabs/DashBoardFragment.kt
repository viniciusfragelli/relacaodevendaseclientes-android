package br.com.viniciusfragellisenff.testemercafacilandroid.Tabs


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.DeshbordVO
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.Filtro
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.InfoDeshBoard

import br.com.viniciusfragellisenff.testemercafacilandroid.R
import br.com.viniciusfragellisenff.testemercafacilandroid.Tabs.adapter.DeshAdapter
import br.com.viniciusfragellisenff.testemercafacilandroid.service.Provider
import br.com.viniciusfragellisenff.testemercafacilandroid.service.Service
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_dash_board.*
import okhttp3.ResponseBody
import org.jetbrains.anko.support.v4.alert
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayInputStream
import java.io.FileOutputStream

class DashBoardFragment() : Fragment() {

    private val compositeDisposable:CompositeDisposable = CompositeDisposable()
    private var filtro = Filtro();
    private var infoDesh = mutableListOf<InfoDeshBoard>()
    var callbackFiltrar :(() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dash_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvDesh.layoutManager = GridLayoutManager(activity,2)
        taskGetInfoDashBordVendas()
    }

    fun setFiltro(filtro: Filtro){
        this.filtro = filtro
        infoDesh = mutableListOf<InfoDeshBoard>()
        taskGetInfoDashBordVendas()
    }

    private fun taskGetInfoDashBordVendas(){
        llProgress.visibility = View.VISIBLE
        compositeDisposable.add(Provider.getService().vendas(filtro.inicio,filtro.fim).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Vendas"
            info.total = deshVO.total ?: "";
            infoDesh.add(info)
            taskGetInfoDashBordClientes()
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                taskGetInfoDashBordClientes()
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun taskGetInfoDashBordClientes(){
        compositeDisposable.add(Provider.getService().clientes(filtro.inicio,filtro.fim).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Clientes"
            info.total = deshVO.total ?: "";
            infoDesh.add(info)
            taskGetInfoDashBordFaturamento()
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                taskGetInfoDashBordFaturamento()
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun taskGetInfoDashBordFaturamento(){
        compositeDisposable.add(Provider.getService().faturamento(filtro.inicio,filtro.fim).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Faturamento"
            info.total = deshVO.total ?: "";
            infoDesh.add(info)
            taskGetInfoDashBordCidades()
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                taskGetInfoDashBordCidades()
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun taskGetInfoDashBordCidades(){
        compositeDisposable.add(Provider.getService().cidades().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Cidades"
            info.total = deshVO.get(0).total ?: "";
            infoDesh.add(info)
            taskGetInfoDashBordBairros()
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                taskGetInfoDashBordBairros()
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun taskGetInfoDashBordBairros(){
        compositeDisposable.add(Provider.getService().bairros().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Bairros"
            info.total = deshVO.get(0).total ?: "";
            infoDesh.add(info)
            taskGetInfoDashBordEstados()
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                taskGetInfoDashBordEstados()
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun taskGetInfoDashBordEstados(){
        compositeDisposable.add(Provider.getService().estados().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({deshVO ->
            var info = InfoDeshBoard()
            info.tipo = "Estados"
            info.total = deshVO.get(0).total ?: "";
            infoDesh.add(info)
            llProgress.visibility = View.GONE
            rvDesh.adapter = DeshAdapter(DashBoardFragment@ this.activity!!.applicationContext,infoDesh)
        },{ ex ->
            ex.printStackTrace()
            if (ex.message!!.contains("404")){
                llProgress.visibility = View.GONE
                rvDesh.adapter = DeshAdapter(DashBoardFragment@ this.activity!!.applicationContext,infoDesh)
            }else{
                showAlertaErroRequest()
            }
        }))
    }

    private fun showAlertaErroRequest(){
        alert {
            title = "Alerta"
            message = "Não foi possível completar a solicitação ao servidor!"
            positiveButton("OK",{})
        }.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}
