package br.com.viniciusfragellisenff.testemercafacilandroid.service

import br.com.viniciusfragellisenff.testemercafacilandroid.Model.DeshbordVO
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by felipe on 01/02/18.
 */
open interface Service {

    companion object {
        var BASIC_AUTH = "Basic "
    }

    @GET("/vendas/count")
    fun vendas(@Query("inicio") inicio: String, @Query("fim") fim: String): Single<DeshbordVO>

    @GET("/vendas/faturamento")
    fun faturamento(@Query("inicio") inicio: String, @Query("fim") fim: String): Single<DeshbordVO>

    @GET("/clientes/count")
    fun clientes(@Query("inicio") inicio: String, @Query("fim") fim: String): Single<DeshbordVO>

    @GET("/geo/estados/count")
    fun estados(): Single<List<DeshbordVO>>

    @GET("/geo/bairros/count")
    fun bairros(): Single<List<DeshbordVO>>

    @GET("/geo/cidades/count")
    fun cidades(): Single<List<DeshbordVO>>

}