package br.com.viniciusfragellisenff.testemercafacilandroid

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import android.support.design.widget.TabLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import br.com.viniciusfragellisenff.testemercafacilandroid.Adapters.PageAdapeter
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.Filtro
import br.com.viniciusfragellisenff.testemercafacilandroid.Tabs.DashBoardFragment
import br.com.viniciusfragellisenff.testemercafacilandroid.Tabs.GraficosFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()
    private lateinit var dashboard: DashBoardFragment

    companion object {
        var filtro = Filtro();
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Mercafacil - Teste"
        setupTabView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filtrar,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_filtrar -> {
                var intent = Intent(this, FiltroActivity::class.java)
                intent.putExtra("filtro", filtro)
                startActivityForResult(intent,1)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                dashboard.setFiltro(filtro)
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun setupTabView(){
        val adapter = PageAdapeter(supportFragmentManager)
        dashboard = DashBoardFragment()
        adapter.adicionar(dashboard, "Dashboard")
        adapter.adicionar(GraficosFragment(), "Gráficos")

        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear();
    }
}
