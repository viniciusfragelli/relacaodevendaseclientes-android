package br.com.viniciusfragellisenff.testemercafacilandroid.service


import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by felipe on 07/02/18.
 */
open class Provider {
    companion object {

        private var service: Service? = null

        fun getService(): Service {
            return service ?: createProvider()
        }

        private fun createProvider() : Service{
            var retrofit = Retrofit.Builder()
                    .baseUrl("http://192.168.88.121:3000")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            var ser = retrofit.create(Service::class.java)
            service = ser
            return ser
        }
    }
}