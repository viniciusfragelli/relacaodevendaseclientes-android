package br.com.viniciusfragellisenff.testemercafacilandroid.Tabs.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.viniciusfragellisenff.testemercafacilandroid.Model.InfoDeshBoard
import br.com.viniciusfragellisenff.testemercafacilandroid.R
import kotlinx.android.synthetic.main.item_adapter.view.*

class DeshAdapter(private val context: Context, private val lista: List<InfoDeshBoard>) : RecyclerView.Adapter<DeshViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DeshViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_adapter, p0, false)
        return DeshViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(view: DeshViewHolder, position: Int) {
        val info = lista[position]
        with(view.itemView){
            tvTipo.text = info.tipo
            tvQuantidade.text = info.total
        }
    }

}

class DeshViewHolder(view: View) : RecyclerView.ViewHolder(view) {

}